Commit phase can be : 

```
# convert markdown to PDF:

ls -1 *.md | xargs -n 1 -I III pandoc III -o III.pdf

#commit============
git commit -a

bumpversion patch
# bumpversion minor
# if going to publish via PyPI:
bumpversion release

#push==============
git push origin master
```
