# SRIM MODE

 *it is assumed here that everything is already  installed*

 *to generate pdf, do `pandoc README_SRIM.md -o README_SRIM.pdf `*
 
## 1st test
Run ` ./test/commandline_srim`. It will simulate alphas going throuh
- one C layer of micrometer thickness
- two layers - C and Si 
- three different microgram/cm2 thicknesses
- creates two new files in `/tmp`
- extracts the initial, final and de energies and prints on screen and makes comparison
- makes the test of extraction 'e' values for two files
- plotting test can be launged by user


## Graphs:

`x,y,z,yz,xz,xy,cos,(cosyz),cosy,cosx,dee,view,(list),e`

`dee` plot has extra color option - different files have different colors


## Examples
```
echo "TELECOPE SIMULATION ================================"
echo "==  he3 and h2 through 500um Si ===================="
sleep 3
nuphy srim -i he3 -m si -e 27 -t   500um -n 500 -s -S /tmp/testtelee.hdf5
nuphy srim -i he3 -m si -e 26 -t   500um -n 500 -s -S /tmp/testtelee.hdf5
nuphy srim -i h2  -m si -e 20 -t   500um -n 500 -s -S /tmp/testteler.hdf5
nuphy srim -i h2  -m si -e 19 -t   500um -n 500 -s -S /tmp/testteler.hdf5
nuphy srim -i h2  -m si -e 18 -t   500um -n 500 -s -S /tmp/testteler.hdf5
```

`nuphy hdf5  -S /tmp/testtelee.hdf5,all,/tmp/testteler.hdf5,all  -g  dee -f 0.1 --savefig pics/de_e.png`

![example](pics/de_e.png)
