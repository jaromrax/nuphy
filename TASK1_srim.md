

## Simulations of protons from 23Ne

 * using kinematic calculator (`nuphy`)
 * using SRIM code
 * `nuphy` package helps to automate SRIM tasks
 * drawing results - fast draw with from `nuphy`, look at possibility to get hdf5 data to ROOT (maybe PyRoot?)
 
 
 
### Tasks

 * select experimental data set : angles (detectors)
 
 * find the excited levels of 23Ne in databases
 
 * calculate kinematics of the outgoing protons from different 23Ne states
 
 * make the simulations with SRIM saving into the hdf5 file(s)
 
 * try different FWHM given by the detector (50,100keV)
 
 * for more complete picture, make the same for elastic scattering of deuteron (d,d reaction)
 
 * compare to experimental picture - small vs. large angles should show eventual impurities
 
 * compare with the test run on air
 
 * the same with air, guess what all is seen in the air - 16O,14N +++ ?




`~/ne22.hdf5` is an arbitrary new fle.


```
#
#  reaction d,p with  deuteron energy 19 MeV at angle 17 deg
#
./bin/nuphy react -i h2,ne22  -o h1  -e 19  -a 17

# simulations   e.g.
#    with a realistic detector FWHM 0.05 MeV    -f 0.05
#    test a realistic detector FWHM 0.1  MeV    -f 0.1
#
nuphy srim  -i h1  -m si  -e 23.6    -t 500um -n 500 -s -S ~/ne22.hdf5 -f 0.1
#
nuphy srim  -i h1  -m si  -e 28    -t 500um -n 500 -s -S ~/ne22.hdf5
nuphy srim  -i h1  -m si  -e 26    -t 500um -n 500 -s -S ~/ne22.hdf5
nuphy srim  -i h1  -m si  -e 24    -t 500um -n 500 -s -S ~/ne22.hdf5
nuphy srim  -i h1  -m si  -e 23.8    -t 500um -n 500 -s -S ~/ne22.hdf5
nuphy hdf5 -S ~/ne22.hdf5
nuphy hdf5
nuphy hdf5 -S ~/ne22.hdf5,0,1,2,3 -g dee
```
